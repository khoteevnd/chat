﻿<?php
$message = '';
error_reporting(E_ALL ^ E_NOTICE);

include("config.php");
include("php/Session.php");
include("php/Utilites.php");
include("php/Storage.php");
include("php/Validation.php");
include("php/Response.php");
include("php/Singleton.php");

$session = new Session();
$data = $_POST;
$utils = new Utilites();
$storage = new StorageFile(PATH_DBFILE.FILE_LOG);


if(!empty($_SESSION) and isset($_SESSION["login"]) and $_SESSION["loging"] == 1)
{
    if (!empty($_POST))
    {
        //Проверка валидации
        $validation = new Validation($data);
        $validation->required();
        $validation->minlen(1,'message');
        $validation->no_characters(NO_CHAR,'login');

        if ($validation->getResult() == true)
        {
            if(isset($data["send"]) and $data["send"] == "post")
            {
                $data["id"] = null;
                $data["time"] = time();
                $data["id"] = $data["time"];
                //$data

                $storage->set_Storage($data, $send = "post");
                $utils->redirectSelf();
            }
            if(isset($data["send"]) and $data["send"] == "coment")
            {
                settype($data["id"],"integer");
                $data["time"] = time();

                $storage->set_Storage($data, $send = "coment");
                $utils->redirectSelf();
            }
        }
        else
        {
            foreach($validation->getErrors() as $err)
            {
                $message = $message.$err.PHP_EOL;
            }
            die($message);
        }
    }
}
else
{
    $utils->redirect(FILE_PHP_LOGIN);
}


$response = new Response();
$html = $response->render(FILE_VIEW, $storage->get_Storage());
printf($html);
//var_dump($_SESSION);
