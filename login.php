<?php
$message = '';
error_reporting(E_ALL ^ E_NOTICE);
include("config.php");

include("php/Session.php");
include("php/Utilites.php");
include("php/Storage.php");
include("php/Validation.php");
include("php/Response.php");

$session = new Session();
$data = $_POST;
$utils = new Utilites();
$storage = new StorageFile(PATH_DBFILE.FILE_USER);

if($session->get("loging") == 1 and $session->get("auth_type") == 1)
{
    $utils->redirect(FILE_PHP_INDEX);
}
else
{
    if(!empty($_POST))
    {
        $stor = $storage->get_Storage();
        $flag = false;
        foreach($stor as $key)
        {
            if($key["login"] == $data["login"] and $key["password"] == $data["password"])
            {
                $flag = true;
                $session->set("loging", 1);
                $session->set("auth_type", 1);
                $session->set("login", $data["login"]);
                break;
            }
        }
        if($flag == true)
        {
            $utils->redirect("index.php");
        }
        else
        {
            $message = "No correct login or password";
            //$utils->redirectSelf();
        }
    }
}
$response = new Response();
$html = $response->render(PATH_VIEW.FILE_VIEW_LOGIN, $data);
printf($html);