<?php
/**
 * Created by PhpStorm.
 * User: Nikolya
 * Date: 12.03.2015
 * Time: 23:03
 */
//DB File
define("PATH_DBFILE", "db/");
define("FILE_LOG", "log");
define("FILE_USER", "user");
//Global message
define("MY_MESSAGE", "");

//Validation rules
define("NO_CHAR", '~`!@#$%^&*()+={}[]\|/<>?,.":;№');


define("FILE_VIEW", "view.php");
define("DATE_FORMAT","H:i:s d-m-Y");
define("FILE_VIEW_REGISTER", "register.php");
define("FILE_VIEW_LOGIN", "login.php");
define("FILE_PHP_INDEX", "index.php");
define("FILE_PHP_LOGIN", "login.php");
define("FILE_PHP_REGISTER", "register.php");
define("FILE_PHP_LOGOUT", "logout.php");
define("PATH_VIEW", "view/");
define("PATH_PHP", "php/");

define("SESSION_NAME", "mysession");

//define("MESSAGE", "");