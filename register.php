<?php
$message = '';
error_reporting(E_ALL ^ E_NOTICE);
include("config.php");

include("php/Session.php");
include("php/Utilites.php");
include("php/Storage.php");
include("php/Validation.php");
include("php/Response.php");

$session = new Session();
$data = $_POST;
$utils = new Utilites();
$storage = new StorageFile(PATH_DBFILE.FILE_USER);

if($session->get("loging") == 1 and $session->get("auth_type") == 1)
{
    $utils->redirect(FILE_PHP_INDEX);
}
else
{
    if(!empty($_POST))
    {
        //Проверка валидации
        $validation = new Validation($data);
        $validation->required();
        $validation->minlen(3,'login');
        $validation->minlen(3,'password');
        $validation->no_characters(NO_CHAR,'login');


        if($validation->getResult() == true)
        {

            if($data["password"] === $data["password_confirm"])
            {
                $storage->set_Storage($data);
                $session->set("loging", 1);
                $session->set("auth_type", 1);
                $session->set("login", $data["login"]);
                $utils->redirect(FILE_PHP_INDEX);
            }
        }
        else
        {
            foreach($validation->getErrors() as $err)
            {
                $message = $message.$err.PHP_EOL;
            }
            die($message);
        }
    }
}



$response = new Response();
$html = $response->render(PATH_VIEW.FILE_VIEW_REGISTER, $data);
printf($html);