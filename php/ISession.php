<?php
interface ISession {
    public function set($key, $value);
    public function get($key);
    public function clear();
    // по желанию
    public function check($key);
    public function setName($name); // имя сессии
}