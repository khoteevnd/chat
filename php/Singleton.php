<?php /** Created by PhpStorm. User: PC Date: 19.03.15 Time: 18:39 */

class Singleton {
    //Храниться образ объекта
    private static $Inst;
    public $FILE_LOG;
    public $A;
    public  $MY_MESSAGE = "";
    public  $NO_CHAR = '~`!@#$%^&*()+={}[]\|/<>?,.":;№';
    public  $PATH_VIEW = "view.php";
    public  $DATE_FORMAT = "H:i:s d-m-Y";
    private function __construct(){}
    private function __clone(){}
    /* @return Singleton */ //type hiting
    public static function getInstace()
    {
        if(!self::$Inst)
        {
            self::$Inst = new Singleton(); //new self::$Inst;
        }
        return new self::$Inst;
    }
    public static function putLog($file_log)
    {
        self::$FILE_LOG = $file_log;
    }
    public  static  function  getLog()
    {
        return self::$FILE_LOG;
    }


}

//Singleton::getInstace()->
//$b = $a::getLog();
//echo $c = $a::$A;
//var_dump($a);
//var_dump($c);

//class Singlton
//private static $Inst;
//public $b;
//private func __constructor()
//public static getInstance()
//{
//    if(!self::$Inst)
//        self::$Inst::new self();
//    return new self::$Inst;
//}