<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 13.03.15
 * Time: 21:36
 */
class A
{
    function foo()
    {
        if (isset($this)) {
            echo '$this определена (';
            echo get_class($this);
            echo ")\n";
        } else {
            echo "\$this не определена.\n";
        }
    }
}
$a = new A;

$a->foo();