<?php
abstract class AStorage
{
    abstract function get_Storage();
    abstract function set_Storage($data);
}
class StorageFile extends AStorage {

    public   $file_name = "";
    private  $date_format = "";
    private  $item_name = "";

    function __construct($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * @param string $item_name
     */
    public function set_Item_Name($item_name)
    {
        $this->item_name = $item_name;
    }

    /**
     * @return string
     */
    public function get_Item_Name()
    {
        return $this->item_name;
    }

    /**
     * @param mixed $date_format
     */
    public function set_Date_Format($date_format)
    {
        $this->date_format = $date_format;
    }

    /**
     * @return mixed
     */
    public function get_Date_Format()
    {
        return $this->date_format;
    }

    /**
     * @param mixed $file_name
     */
    public function set_File_Name($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * @return mixed
     */
    public function get_File_Name()
    {
        return $this->file_name;
    }

    public function get_Storage()
    {
        $h = fopen($this->file_name, "r+");
        $data = [];
        while ($line = fgets($h)) {
            $data[] = json_decode($line, true);
        }
        fclose($h);
        return $data;
    }
    protected function date_Output_Format(&$data, $item_name)
    {
        foreach ($data as &$item) {
            $date = $item[$item_name];
            $item[$item_name] = $date;
        }
        unset($item);
        unset($data);
        //return $data;
    }
    public function set_Storage($data)
    {
        $text = json_encode($data);
        file_put_contents($this->get_File_Name(), $text . PHP_EOL, FILE_APPEND);
    }
}