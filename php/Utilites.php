<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 16.03.15
 * Time: 18:48
 */

class Utilites {
    public function redirect($url)
    {
        header("location: $url");
        die();
    }

    public function redirectSelf()
    {
        $u = $_SERVER['REQUEST_URI'];
        $this->redirect($u);
    }
}