<?php
include("ISession.php");

class Session implements ISession {
    public function __construct()
    {
        session_start();
    }
    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }
    public function get($key)
    {
        return $_SESSION[$key];
    }
    public function clear()
    {
        session_unset();
        session_destroy();
    }
    public function check($key)
    {
        if(!empty($_SESSION) and isset($_SESSION[$key]))
        return true;
        return false;
    }
    public function setName($name)
    {
        session_name($name);
    }
} 