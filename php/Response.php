<?php
class Response {
    public function render($path, $data = null){
        //var_dump($data);
        ob_start();
        if($data != null)
            extract($data);
        include $path;
        //var_dump($data);
        $html = ob_get_clean();
        return $html;
    }
}