<?php
//Получение списка из вайла////////////////////////////////////////////////////////////////////
function get()
{
    $h = fopen(FILE_LOG, "r+");
    $arr = array();
    while ($line = fgets($h)) {
        $arr[] = json_decode($line, true);
    }
    fclose($h);
    return $arr;
}
//Добавление новой записи в файл///////////////////////////////////////////////////////////////
function set($data)
{
    $data['time'] = time();
    $text = json_encode($data);
    file_put_contents(FILE_LOG, $text . PHP_EOL, FILE_APPEND);
}
//Перенаправление броузера на туже страницу////////////////////////////////////////////////////
function location()
{
    header('location: index.php');
    //exit;
}
//Написать валидатор данных, проверка на пустые строки, на запрещенные символы (придумать самому)
